// Copyright Mateusz Wojt, 2021. All rights reserved.

#ifdef _WIN32
#pragma warning(disable: 4996)
#endif

#include <iostream>
#include <iomanip>

#include "MyPlugin.h"

MyPluginIop::MyPluginIop(Node* node) : Iop(node)
{
// initialize all class members here
};

void MyPluginIop::knobs(Knob_Callback f)
{
// add knobs here
}

const char* MyPluginIop::input_label(int n, char*) const
{
	return 0;
}

void MyPluginIop::_validate(bool for_real)
{
// basic validation
	copy_info();
	merge_info();
}

void MyPluginIop::_request(int x, int y, int r, int t, ChannelMask channels, int count)
{
// request channels from first input
	input(0)->request(x, y, r, t, channels, count);
}

void MyPluginIop::_open()
{
// _open call implementation
}

void MyPluginIop::engine(int y, int x, int r, ChannelMask channels, Row& out)
{
// engine call implementation
}

static Iop* build(Node* node) { return new MyPluginIop(node); }
const Iop::Description MyPluginIop::d("MyPlugin", "Other/MyPlugin", build);