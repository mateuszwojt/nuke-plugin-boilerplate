// Copyright Mateusz Wojt, 2021. All rights reserved.

#ifndef MYPLUGIN_H
#define MYPLUGIN_H

#include "DDImage/Iop.h"
#include "DDImage/Knobs.h"
#include "DDImage/Knob.h"
#include "DDImage/DDMath.h"

static const char* const HELP = "CPU/GPU CG render denoiser based on Intel OpenImageDenoise and NVidia Optix libraries.";
static const char* const CLASS = "Denoiser";

using namespace DD::Image;

class MyPluginIop : public Iop
{
public:
// constructor
    MyPluginIop(Node* node);

// Nuke internal methods
	int minimum_inputs() const { return 1; }
	int maximum_inputs() const { return 1; }

	void knobs(Knob_Callback f);

	void _validate(bool);
	void _request(int x, int y, int r, int t, ChannelMask channels, int count);
	void _open();
	void engine(int y, int x, int r, ChannelMask channels, Row& out);

	const char* input_label(int n, char*) const;
    static const Iop::Description d;

	const char* Class() const { return CLASS; }
	const char* node_help() const { return HELP; }

// private class members
private:
// public class memebers
public:
};

#endif // MYPLUGIN_H